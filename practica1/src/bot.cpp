#include <iostream>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>

#include "DatosMemCompartida.h"
#include "Esfera.h"
#include "Raqueta.h"

int main()

{
	//Creación del fichero,los punteros a la memoria y a la proyeccion
	int f;
	DatosMemCompartida* pMemComp;
	char* proyeccion;

	//Apertura el fichero
	f=open("/tmp/datosBot.txt",O_RDWR);
	if(f==-1) {
		printf("Error al abrir el Bot\n");
		exit(-1);
	}

	//Proyección el fichero
	proyeccion=(char*)mmap(NULL,sizeof(*(pMemComp)),PROT_WRITE|PROT_READ,MAP_SHARED,f,0);

	close(f);

	//Apunto el puntero de Datos a la proyeccion del fichero en memoria
	pMemComp=(DatosMemCompartida*)proyeccion;

	//Control de la raqueta
	while(1)
	{
		usleep(10000);
		float posRaqueta1;
		
		posRaqueta1=((pMemComp->raqueta1.y2+pMemComp->raqueta1.y1)/2);
		
		if(posRaqueta1<pMemComp->esfera.centro.y)
			pMemComp->accion1=1;
		else if(posRaqueta1>pMemComp->esfera.centro.y)
			pMemComp->accion1=-1;
		else
			pMemComp->accion1=0;	
		
	}

	//Desproyeccion
	munmap(proyeccion,sizeof(*(pMemComp)));
	return 0;

}
